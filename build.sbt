name := "spark_test"

version := "1.0"

scalaVersion := "2.10.5"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.4.1"