## Spark questions
# Write a distributed program (using the Apache Spark API or similar) to find the average of a potentially infinite (i.e. arbitrarily large) set of longs using a fixed memory footprint
From the Spark API [DoubleRDDFunctions](https://spark.apache.org/docs/1.4.1/api/scala/index.html#org.apache.spark.rdd.DoubleRDDFunctions) 
we can call directly the function mean on an RDD[Double] with a fixed memory foot print.   
Or we can use two accumulators, one to sum and the other one to count the number of values, then the average is sum/count
See InifinteAverage.sc worksheet. 

```scala 
         val values = sc.parallelize(Array(1L, 2L, 3L, 4L)) 
         val sum = sc.accumulator(0.0) 
         val count = sc.accumulator(0.0) 
         values.foreach( r => { 
            sum += r 
            count += 1 
         })
         ```


# Write a short paragraph discussing the pros and cons of Spark vs traditional Map Reduce
**Spark pros Vs Map Reduce**
- Faster execution (Iterative computation):
  The core of spark is RDD(Resilient Distributed Data) which is a collection of "records" that is distributed or partitioned across multiple nodes.
  In Spark programming model, operations are split into transformation and actions. Transformation operation applies some function to all the records in the dataset. 
  An action runs some computation or aggregation on the dataset and return the result. By combining multiple Transformations, it creates a data lineage where it is faster
  than a typical map reduce in this situation. Spark will use caching and in memory processing while a typical map reduce will have to persistent all the temporary data between each transformation, creating much more I/O operations.
- Streaming and interactive mode:
 Spark can be used easily to use interactive SQL queries, you can argue you can do the same with hive, but you will not have the same speed of execution. 
 Spark contains a myriad of build it libraries to perform Machine learning, easy integration with RStudio, Streaming where with Map Reduce you can do only batch processing.
 
**Spark cons Vs Map Reduce**
- Still work in progress
- Memory usage sometimes can be excessive 

# Write a program to accurately calculate the distance between the following two sites Gatwick Airport (51.153662, -0.182063) and Sydney Opera House (-33.857197, 151.21514)
Using the [Great Circle distance](https://en.wikipedia.org/wiki/Great-circle_distance) to calculate the distance between two coordinates, see GeoLocation.sc 

# Spark exercise 
Given the following example dataset:
   Sally A,B,A,B,B,C,A,B,D,A
   Fred B,A,A,A,B,C,D,A,C,A
   Sally B,B,B,A,A,A,B,C,A
   John A,B,A,B,F,H,B,A,B,B
Write a distributed program (using the Apache Spark API or similar) to output the data in the following format:
   Sally – A [Count] B [Count]
   Fred – A [Count] B [Count]
   John – A [Count] B [Count]
where Count is the relevant count for that argument. 
 
Assume that any future datasets, although in the same format, may contain arbitrary number of records with an arbitrary length of data points
 
  
  
