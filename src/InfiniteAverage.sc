import org.apache.spark.{SparkContext, SparkConf}
val conf = new SparkConf().setMaster("local").setAppName("test-spark")
val sc = new SparkContext(conf)
val values = sc.parallelize(Array(1L, 2L, 3L, 4L))
val sum = sc.accumulator(0.0)
val count = sc.accumulator(0.0)
values.foreach( r => {
  sum += r
  count += 1
})
val average = sum.value / count.value

