import org.apache.spark.{SparkConf, SparkContext}


val conf = new SparkConf().setMaster("local").setAppName("test-spark")

val sc = new SparkContext(conf)
val dataSetPath = "dataset.txt"
val dataFile = sc.textFile(dataSetPath)
val keep = Set("A", "B")

val parsedLines = dataFile.map(line => (line.split(" ")(0), line.split(" ")(1))).reduceByKey(_ + "," + _)

parsedLines.foreach(l => {
  print("\n" + l._1);
  l._2.split(",")
    .filter(c => keep.contains(c))
    .groupBy(x => x)
    .mapValues {
    _.size
  }.foreach(x => print("\t" + x._1 + "[" + x._2 + "]"))
})
