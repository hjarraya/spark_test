case class Coordinates(latitude: Double, longitude: Double)

object GeoLocation {
  def distanceInKm(c1: Coordinates, c2: Coordinates): Long = {
    (distance(c1, c2) / 1000.00).toLong
  }

  def distance(c1: Coordinates, c2: Coordinates): Double = {
    distance(c1.latitude, c2.latitude, c1.longitude, c2.longitude)
  }

  def distance(lat1: Double, lat2: Double, lon1: Double, lon2: Double): Double = {
    val R: Int = 6371
    val latDistance: Double = deg2rad(lat2 - lat1)
    val lonDistance: Double = deg2rad(lon2 - lon1)
    val a: Double = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
    val c: Double = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    var distance: Double = R * c * 1000
    distance = Math.pow(distance, 2)
    Math.sqrt(distance)
  }


  private def deg2rad(deg: Double): Double = {
    (deg * Math.PI / 180.0)
  }

}

/// Gatwick Airport (51.153662, -0.182063) and Sydney Opera House (-33.857197, 151.21514)
val LGW = Coordinates(51.153662, -0.182063)
val SOH = Coordinates(-33.857197, 151.21514)
val distance = GeoLocation.distanceInKm(LGW, SOH)

